﻿
using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using zoraChat.Models;
using System.Linq;

namespace ZoraChat
{
    public class ChatHub : Hub
    {
        // stores it our clients list
        static readonly ConcurrentDictionary<string, user> _users =
         new ConcurrentDictionary<string, user>();

        public void Join(String userName)
        {
            // populate user
            user usr = new user()
            {
                Name = userName,
                Id = Context.ConnectionId
            };

            // converts to dictionary
            Dictionary<string, user> dict = _users.ToDictionary(w => w.Key, m => m.Value);
            bool duplicate = false;
            // see if a duplicate exists
            if (dict.Where(u => u.Value.Name.Equals(userName)).ToList().Count > 0)
            {
                usr.Name = getRandomName(userName);
                duplicate = true;
            }

            // add user to the dictionary 
            _users.AddOrUpdate(usr.Id, usr, (k, v) => usr);
            // get the total number of connected users.
            int userCounter = _users.Count;
            // update the number of users and the users list
            Clients.All.updateUsers(userCounter, _users.ToArray());
            if (duplicate)
            {
                // change callers username if duplicate
                Clients.Caller.changeUserName(usr.Name);
            }
            // broad cast who has joined to all users
            Clients.All.notify(usr.Name);
        }

        public void Send(string message)
        {
            // get the time
            DateTime time = DateTime.Now;
            // format time
            string displayTime = time.ToString("hh:mm tt");
            // get the userName from the dictionary
            String name = _users[Context.ConnectionId].Name;
            Clients.All.broadcastMessage(name, message, displayTime);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            // remove the user which triggered the disconnect method
             user usr;
             _users.TryRemove(Context.ConnectionId, out usr);
            // get the total number of connected users.
            int userCounter = _users.Count;
            // notify user who has left
            try
            {
                Clients.All.userLeft(usr.Name);
            } catch (Exception)
            {

            }
                
            // update the number of users and the users list
            Clients.All.updateUsers(userCounter, _users.ToArray());
            return base.OnDisconnected(stopCalled);
        }

        private string getRandomName(string userName)
        {
            Random rnd = new Random();
            int rand = rnd.Next(1, 100);
            return userName + "_" + rand;
        } 
    }
}

