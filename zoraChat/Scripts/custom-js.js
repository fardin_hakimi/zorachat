﻿(function () {


    var app = angular.module('zoraChat', ['luegg.directives', 'ngSanitize']);

    app.controller('chatCtr', ["$scope", function ($scope) {
        // scope variables
        $scope.username = ''; // holds the user's name
        $scope.message = ''; // holds the new message
        $scope.messages = []; // collection of messages coming from server
        $scope.usersList = [];
        $scope.loginSwitch = true;
        $scope.chatSwitch = false;
        $scope.appName = "ZoraChat";
        $scope.welcomeMessage = "";
        $scope.onlineUsers = 0;
        $scope.time = "";

        // get the chathub reference
        $scope.chat = $.connection.chatHub;

        // to switch views
        $scope.switchView = function () {
            $scope.loginSwitch = !$scope.loginSwitch;
            $scope.chatSwitch = !$scope.chatSwitch;
        };


        // invoke upon connect request
        $scope.openConnection = function () {
            //start the connection
            $.connection.hub.start(function () {
                // register the user on the server 
                $scope.chat.server.join($scope.username);
            });
            $scope.switchView();
        };
        // end


        // invoked upon enter by user to send the message 
        $scope.sendMessage = function () {
            // Call the Send method on the hub.
            if ($scope.message) {
                $scope.chat.server.send($scope.message);
                // Clear text box
                $scope.message = "";
            }
        };
     // end 

  
   //  invoked by server to broadcast messages
        $scope.chat.client.broadCastMessage = function (username, message, time) {
            // Add the message to the page.
            $scope.time = time;
            $scope.messages.push({
                message: username + " : " + message,
                time:"<span class='pull-right'> " + time + "</span>"
            });
            $scope.$apply();
        };
   // end


        // broadcast that this user has joined
        $scope.chat.client.changeUserName = function (name) {
            $scope.username = name;
            $scope.welcomeMessage = "You are now connected to the chat room";
                $scope.$apply();
        };
     // end

        // broadcast that this user has joined
        $scope.chat.client.notify = function (name) {
            if ($scope.username == name){
                $scope.welcomeMessage = "You are now connected to the chat room";
            } else {
                $scope.welcomeMessage = name + " has joined";
            }
            $scope.$apply();
        };
     // end

        // broadcast that this user has left
        $scope.chat.client.userLeft = function (name) {
            $scope.welcomeMessage = name + " left the chatroom";
            $scope.$apply();
        };
     // end


        // update list of users and user count.
    $scope.chat.client.updateUsers = function (userCount, users) {
            $scope.onlineUsers = userCount;
            $scope.usersList = users;
            $scope.$apply();
        };
     // end
     
    }]);
}());